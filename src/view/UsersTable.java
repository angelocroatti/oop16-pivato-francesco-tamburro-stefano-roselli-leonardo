package view;

import java.util.List;

import model.interfaces.User;

/**
 * 
 * @author Ste
 *
 */
public interface UsersTable {
	
	void setData(List<User> data);

}
